Possibly setup version control with a private remote and bug tracker, GitLab or GitHub.
Playing tennis with an ESP kinda sucks. Can we establish which ESP is the "master" going forward, and them rename/flag it as such.
Then we will have `autodocMk1.esp` and `autodocMk1.esl`. The master ESP will be what you edit in the CK. When your happy with the changes save the ESP and convert active file to light master.   The ESL/ESM is what we can each master our own ESP to in order to purpose changes to the master.

Then we can all work on the plugin with stepping on toes.
`autodocMk1.esl`
^ `autodoc_wanamingo.esp`
^ `autodoc_Flipdeezy.esp`
^ `autodoc_scrivener.esp`
All the various plugins would be for development only.
My plan above means only releasing `autodocMk1.esl` with the `ba2`.
FYI, your not suppose to edit an ESL.
You keep the ESP for editing and "publish" the final ESL.
Dont just have a single data file you flip back and forth.


So I hope at this point no one has any pending ESPs they have been working on?
I wanted to clean the plugin, rename the bed forms, and add flavor texts.
Let me know if you have stuff you want to make it into the "master" plugin.


Going forward for making plugin changes, it will be easiest for everyone to create an all new plugin mastered to `AutoDoc.esl`.
Then we can merge each persons ESP into AutoDoc.esl when ready.
Then no one is ever waiting for their "turn" to edit the plugin.


Master Plugin: https://gitlab.com/Wanamingo/autodocmk1/blob/master/Data/AutoDoc.esl
The original working plugin name was autodocmk1.esp but I purposely change it to AutoDoc.esp and AutoDoc.esl. This is just until we get everyone consolidated.

I didnt want a potential file overwrite happening if one of you submits a plugin.
If we are all good on the plugin and everyone agrees the link above is the "latest" then I can change the name back or leave it as-is.


Yea, install to data like a normal mod. Besides maybe that f4se import I put (unneeded), you should be able to simply double click the `build.bat` to compile the complete autodoc source.


Optionally if you want to use the CK with AutoDoc, configure your `CreationKitCustom.ini` with these values.
```
[Papyrus]
sScriptSourceFolder = ".\Data\Scripts\Source\AutoDoc"
sAdditionalImports ="$(source);.\Data\Scripts\Source\User;.\Data\Scripts\Source\Base"
sAutoFillNamespaceDefault1 = "AutoDoc"
```

Thats only needed if you want to create new scripts in the CK.
Mine doesnt even have special autodoc settings and I can still attach scripts and do properties.


Using the 3ds Max nif plugin to "export with animation" (settings: http://imgur.com/a/DBbL4)
