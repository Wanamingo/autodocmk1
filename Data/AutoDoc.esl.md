**Masters**
```
00  Fallout4.esm
```

**Details**
```
AVIF
  01000802 AutoDoc_Terminal_Powered

COBJ
  01000F9A AutoDoc_Terminal_MK2_Workshop_co

FURN
  01000801 AutoDoc_BedHospital_Interact
  01000800 AutoDoc_BedHospital_Sleep
  01000F9B AutoDoc_ChairVault_Interact

SNDR
  01000804 AutoDoc_Terminal_SFX_Arm
  01000803 AutoDoc_Terminal_SFX_Looping
  01000805 AutoDoc_Terminal_SFX_Start
  01000806 AutoDoc_Terminal_SFX_Stop

TERM
  01000F99 AutoDoc_Terminal_MK2
```
