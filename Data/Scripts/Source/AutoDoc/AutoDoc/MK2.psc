ScriptName AutoDoc:MK2 Extends ObjectReference
{Attaches to the AutoDoc Terminal object.}

Actor Player

; The created furniture reference.
ObjectReference Created


; Terminal -----------------------------------

int OptionServiceSleep = 1 const
int OptionServiceInjuries = 4 const
int OptionServiceCureDisease = 6 const
int OptionServiceDecontaminate = 5 const
int OptionServiceDetoxify = 7 const
int OptionServiceBarber = 8 const
int OptionServiceCosmetic = 9 const
int OptionServiceCybernetics = 3 const

; Animations ---------------------------------

; Used for the bed
string AttachPoint_Bed = "ChairAttachPoint" const

; Animated cylinder containers
string AttachPoint_MedContainer1 = "MedContainer1AttachPoint" const
string AttachPoint_MedContainer2 = "MedContainer2AttachPoint" const
string AttachPoint_MedContainer3 = "MedContainer3AttachPoint" const

; Cabinate container
string AttachPoint_Door = "DoorAttachPoint" const

; For fusioncell required messagebox. Attach an invisible activator.
string AttachPoint_SwitchesBox = "SwitchesBoxAttachPoint" const


; Properties
;---------------------------------------------

Group Properties
	Terminal Property AutoDoc_Terminal_MK2 Auto Const Mandatory
	{This is also the `self` base object.}

	Container Property AutoDoc_Container_CylinderA Auto Const Mandatory
	Container Property AutoDoc_Container_CylinderB Auto Const Mandatory
	Container Property AutoDoc_Container_CylinderC Auto Const Mandatory
EndGroup


Group Services
	AutoDoc:Cybernetics Property Cybernetics Auto Const Mandatory

	Keyword Property AutoDoc_Service_Sleep Auto Const Mandatory
	Keyword Property AutoDoc_Service_MedicalInjury Auto Const Mandatory
	Keyword Property AutoDoc_Service_MedicalDecontaminate Auto Const Mandatory
	Keyword Property AutoDoc_Service_MedicalDetoxify Auto Const Mandatory
	Keyword Property AutoDoc_Service_MedicalCure Auto Const Mandatory
	Keyword Property AutoDoc_Service_Barber Auto Const Mandatory
	Keyword Property AutoDoc_Service_Cosmetic Auto Const Mandatory
	Keyword Property AutoDoc_Service_Cybernectics Auto Const Mandatory

	Furniture Property AutoDoc_BedHospital_Sleep Auto Const Mandatory
	{The furniture for "Sleep" only.}

	Furniture Property AutoDoc_BedHospital_Medical Auto Const Mandatory
	{The furniture for interacting while laying down.}

	Furniture Property AutoDoc_BedHospital_Barber Auto Const Mandatory
	{The furniture for "Barber" only.}

	Furniture Property AutoDoc_BedHospital_Cosmetic Auto Const Mandatory
	{The furniture for "Surgery" only.}

	Furniture Property AutoDoc_BedHospital_Cybernectic Auto Const Mandatory
	{The furniture for "Cybernetics" only.}
EndGroup




; Events
;---------------------------------------------

Event OnInit()
	{https://www.creationkit.com/fallout4/index.php?title=OnInit_-_ScriptObject}
	Debug.Trace("\n\n")
	Debug.TraceSelf(self, "OnInit", "Event has been received.")
	Player = Game.GetPlayer()
EndEvent


Event OnLoad()
	{https://www.creationkit.com/fallout4/index.php?title=OnLoad_-_ObjectReference}
	Debug.TraceSelf(self, "OnLoad", "Event has been received.")

	While (Is3DLoaded() == false)
		Utility.Wait(1)
		Debug.TraceSelf(self, "OnLoad", "Cannot disable Havok. Waiting for 3D to be loaded.")
	EndWhile


	self.PlaceAtNode(AttachPoint_MedContainer1, AutoDoc_Container_CylinderA)
	self.PlaceAtNode(AttachPoint_MedContainer2, AutoDoc_Container_CylinderB)
	self.PlaceAtNode(AttachPoint_MedContainer3, AutoDoc_Container_CylinderC)


	; Add the default sleep bed.
	Create(AutoDoc_BedHospital_Sleep)

	RegisterForRemoteEvent(AutoDoc_Terminal_MK2, "OnMenuItemRun") ; TODO: only listen to Terminal after activation
EndEvent


Event OnUnload()
	{https://www.creationkit.com/fallout4/index.php?title=OnUnload_-_ObjectReference}
	Debug.TraceSelf(self, "OnUnload", "Event has been received.")
	UnregisterForRemoteEvent(AutoDoc_Terminal_MK2, "OnMenuItemRun")
EndEvent


; Attach
;---------------------------------------------

bool Function Create(Furniture object)
	Dispose()
	Created = self.PlaceAtNode(AttachPoint_Bed, object, 1, true)
	If (Created)
		RegisterForRemoteEvent(Created, "OnActivate")
		Debug.TraceSelf(self, "Create", "Created the "+Created+" furniture reference.")
		return true
	Else
		Debug.TraceSelf(self, "Create", "Failed to create "+object+" furniture reference.")
		return false
	EndIf
EndFunction


bool Function Dispose()
	If (Created)
		Debug.TraceSelf(self, "Dispose", "Disposing of the "+Created+" reference.")
		UnregisterForRemoteEvent(Created, "OnActivate")
		Created.Delete()
		Created = none
		return true
	Else
		return false
	EndIf
EndFunction


;---------------------------------------------

Event Terminal.OnMenuItemRun(Terminal sender, int menuItemID, ObjectReference terminalReference)
	If (terminalReference != self)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The reference was unhandled. Created:"+Created)
		return
	EndIf

	If (menuItemID == OptionServiceSleep)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Sleep' option was selected.")
		If (Create(AutoDoc_BedHospital_Sleep))
			Created.AddKeyword(AutoDoc_Service_Sleep)
		EndIf

	ElseIf (menuItemID == OptionServiceInjuries)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Medical Injury Service' option was selected.")
		If (Create(AutoDoc_BedHospital_Medical))
			Created.AddKeyword(AutoDoc_Service_MedicalInjury)
		EndIf

	ElseIf (menuItemID == OptionServiceDecontaminate)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Medical Decontamination Service' option was selected.")
		If (Create(AutoDoc_BedHospital_Medical))
			Created.AddKeyword(AutoDoc_Service_MedicalDecontaminate)
		EndIf

	ElseIf (menuItemID == OptionServiceCureDisease)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Medical Cure Disease Service' option was selected.")
		If (Create(AutoDoc_BedHospital_Medical))
			Created.AddKeyword(AutoDoc_Service_MedicalCure)
		EndIf

	ElseIf (menuItemID == OptionServiceDetoxify)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Medical Detoxify Service' option was selected.")
		If (Create(AutoDoc_BedHospital_Medical))
			Created.AddKeyword(AutoDoc_Service_MedicalDetoxify)
		EndIf

	ElseIf (menuItemID == OptionServiceBarber)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Barber Service' option was selected.")
		If (Create(AutoDoc_BedHospital_Barber))
			Created.AddKeyword(AutoDoc_Service_Barber)
		EndIf

	ElseIf (menuItemID == OptionServiceCosmetic)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Cosmetic Service' option was selected.")
		If (Create(AutoDoc_BedHospital_Cosmetic))
			Created.AddKeyword(AutoDoc_Service_Cosmetic)
		EndIf

	ElseIf (menuItemID == OptionServiceCybernetics)
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The 'Cybernetics Service' option was selected.")
		If (Create(AutoDoc_BedHospital_Cybernectic))
			Created.AddKeyword(AutoDoc_Service_Cybernectics)
		EndIf

	Else
		Debug.TraceSelf(self, "Terminal.OnMenuItemRun<sender:"+sender+", menuItemID:"+menuItemID+", terminal:"+terminalReference+">", "The option was unhandled.")
		Create(AutoDoc_BedHospital_Sleep)
	EndIf
EndEvent


Event ObjectReference.OnActivate(ObjectReference sender, ObjectReference actionReference)
	Actor patient = actionReference as Actor
	If (!patient)
		Debug.TraceSelf(self, "ObjectReference.OnActivate<sender:"+sender+", actionReference:"+actionReference+">", "Error, the action reference must be an Actor type.")
		return
	EndIf

	If (sender.HasKeyword(AutoDoc_Service_Sleep))
		Debug.TraceSelf(self, "ObjectReference.OnActivate<sender:"+sender+", patient:"+patient+">", "Sleep")
		; standard activation will open the menu
		return

	ElseIf (sender.HasKeyword(AutoDoc_Service_Barber))
		Debug.TraceSelf(self, "ObjectReference.OnActivate<sender:"+sender+", patient:"+patient+">", "Barber")
		; standard activation will open the menu
		; add "feelin fresh buff"
		return

	ElseIf (sender.HasKeyword(AutoDoc_Service_Cosmetic)) ; cosmetic
		Debug.TraceSelf(self, "ObjectReference.OnActivate<sender:"+sender+", patient:"+patient+">", "Surgery")
		; standard activation will open the menu
		; add "sore face debuff"
		return

	ElseIf (sender.HasKeyword(AutoDoc_Service_Cybernectics))
		Debug.TraceSelf(self, "ObjectReference.OnActivate<sender:"+sender+", patient:"+patient+">", "Cybernectics")
		; standard activation will open the menu
		; add "dazed debuff"
		return

	Else
		;Medical interaction bed
		Utility.Wait(5)
		Game.FadeOutGame(true, true, 0.0, 0.5, abStayFaded=true)

		AnimateAndWait("furnitureOn", "TransitionComplete")
		Medical(sender, patient)

		Utility.Wait(2.0)
		Game.FadeOutGame(false, true, 0.0, 0.5)
	EndIf
EndEvent


Function Medical(ObjectReference bed, Actor patient)
	If (bed.HasKeyword(AutoDoc_Service_MedicalInjury))
		Debug.TraceSelf(self, "Medical<bed:"+bed+", patient:"+patient+">", "Heal Injury")
		HealInjuries(patient)
	EndIf

	If (bed.HasKeyword(AutoDoc_Service_MedicalDecontaminate))
		Debug.TraceSelf(self, "Medical<bed:"+bed+", patient:"+patient+">", "Heal Radiation")
		HealRadiation(patient)
	EndIf

	If (bed.HasKeyword(AutoDoc_Service_MedicalDetoxify))
		Debug.TraceSelf(self, "Medical<bed:"+bed+", patient:"+patient+">", "Heal Addiction")
		HealAddiction(patient)
	EndIf

	If (bed.HasKeyword(AutoDoc_Service_MedicalCure))
		Debug.TraceSelf(self, "Medical<bed:"+bed+", patient:"+patient+">", "Heal Disease")
		HealDisease(patient)
	EndIf
EndFunction


; Workshop
;---------------------------------------------

Event OnWorkshopObjectMoved(ObjectReference reference)
	{https://www.creationkit.com/fallout4/index.php?title=OnWorkshopObjectMoved_-_ObjectReference}
	Debug.TraceSelf(self, "OnWorkshopObjectMoved<reference:"+reference+">", "Event has been received.")
	Create(AutoDoc_BedHospital_Sleep)

	; self.MoveToNode(containerReferenceA, AttachPoint_MedContainer1)
	; self.MoveToNode(containerReferenceB, AttachPoint_MedContainer2)
	; self.MoveToNode(containerReferenceC, AttachPoint_MedContainer3)
EndEvent

Event OnWorkshopObjectRepaired(ObjectReference reference)
	{https://www.creationkit.com/fallout4/index.php?title=OnWorkshopObjectRepaired_-_ObjectReference}
	Debug.TraceSelf(self, "OnWorkshopObjectRepaired<reference:"+reference+">", "Event has been received.")
	Create(AutoDoc_BedHospital_Sleep)
EndEvent

Event OnWorkshopObjectGrabbed(ObjectReference reference)
	{https://www.creationkit.com/fallout4/index.php?title=OnWorkshopObjectGrabbed_-_ObjectReference}
	Debug.TraceSelf(self, "OnWorkshopObjectGrabbed<reference:"+reference+">", "Event has been received.")
	Dispose()
EndEvent

Event OnDestructionStageChanged(int oldStage, int currentStage)
	{https://www.creationkit.com/fallout4/index.php?search=OnDestructionStageChanged}
	Debug.TraceSelf(self, "OnDestructionStageChanged<oldStage:"+oldStage+", currentStage:"+currentStage+">", "Event has been received.")
	If (currentStage != 0)
		Dispose()
	EndIf
EndEvent

Event OnWorkshopObjectDestroyed(ObjectReference reference)
	{https://www.creationkit.com/fallout4/index.php?title=OnWorkshopObjectDestroyed_-_ObjectReference}
	Debug.TraceSelf(self, "OnWorkshopObjectDestroyed<reference:"+reference+">", "Event has been received.")
	Dispose()
EndEvent


; Animation
;---------------------------------------------

bool Function Animate(string animation)
	If (self.PlayAnimation(animation))
		Debug.TraceSelf(self, "Animate<"+animation+">", "The request was sent successfully.")
		return true
	Else
		Debug.TraceSelf(self, "Animate<"+animation+">", "The request failed to send.")
		return false
	EndIf
EndFunction


bool Function AnimateAndWait(string animation, string eventName)
	If (self.PlayAnimationAndWait(animation, eventName))
		Debug.TraceSelf(self, "AnimateAndWait<"+animation+", "+eventName+">", "The request was sent successfully.")
		return true
	Else
		Debug.TraceSelf(self, "AnimateAndWait<"+animation+", "+eventName+">", "The request failed to send.")
		return false
	EndIf
EndFunction




; See GenericDoctorScript.psc
; https://www.creationkit.com/fallout4/index.php?title=ResetHealthAndLimbs_-_Actor

; Health
;---------------------------------------------

Group Health
	ActorValue Property Health Auto Const Mandatory
	{The health attribute.}

	ActorValue Property PerceptionCondition Auto Const Mandatory
	{Limb Head}

	ActorValue Property EnduranceCondition Auto Const Mandatory
	{Limb Torso}

	ActorValue Property LeftAttackCondition Auto Const Mandatory
	{Limb Left Arm}

	ActorValue Property LeftMobilityCondition Auto Const Mandatory
	{Limb Left Leg}

	ActorValue Property RightAttackCondition Auto Const Mandatory
	{Limb Right Arm}

	ActorValue Property RightMobilityCondition Auto Const Mandatory
	{Limb Right Leg}
EndGroup


Function HealInjuries(Actor patient)
	patient.RestoreValue(Health, 9999)
	patient.RestoreValue(PerceptionCondition, 9999)
	patient.RestoreValue(EnduranceCondition, 9999)
	patient.RestoreValue(LeftAttackCondition, 9999)
	patient.RestoreValue(LeftMobilityCondition, 9999)
	patient.RestoreValue(RightAttackCondition, 9999)
	patient.RestoreValue(RightMobilityCondition, 99999)
	; patient.ResetHealthAndLimbs()
EndFunction


; Disease
;---------------------------------------------

Potion Property HC_Antibiotics_SILENT_SCRIPT_ONLY Auto Const Mandatory
{This doesn't have an audio effect. Used only for clearing disease when doctors heal.}

Function HealDisease(Actor patient)
	;for new survival - curing diseases
	If patient == Game.GetPlayer()
		patient.EquipItem(HC_Antibiotics_SILENT_SCRIPT_ONLY, abSilent = true)
	EndIf
EndFunction


; Radiation
;---------------------------------------------

ActorValue Property Rads Auto Const Mandatory

Function HealRadiation(Actor patient)
	int amount = patient.GetValue(Rads) as int
	patient.RestoreValue(Rads, amount)
EndFunction


; Addictions
;---------------------------------------------

Spell Property CureAddictions Auto Const Mandatory

Function HealAddiction(Actor patient)
	CureAddictions.Cast(patient, patient)
EndFunction
