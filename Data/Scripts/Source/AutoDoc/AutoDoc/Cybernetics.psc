ScriptName AutoDoc:Cybernetics Extends Quest

Actor Player

int ImplantSlot_Head = 1
int ImplantSlot_Body = 1
int ImplantSlot_ArmLeft = 1
int ImplantSlot_ArmRight = 1
int ImplantSlot_LegLeft = 1
int ImplantSlot_LegRight = 1


; Properties
;---------------------------------------------

Group Properties
	MiscObject Property AutoDoc_Cyber_ImplantSlot_Head Auto Const Mandatory
	MiscObject Property AutoDoc_Cyber_ImplantSlot_Body Auto Const Mandatory
	MiscObject Property AutoDoc_Cyber_ImplantSlot_ArmLeft Auto Const Mandatory
	MiscObject Property AutoDoc_Cyber_ImplantSlot_ArmRight Auto Const Mandatory
	MiscObject Property AutoDoc_Cyber_ImplantSlot_LegLeft Auto Const Mandatory
	MiscObject Property AutoDoc_Cyber_ImplantSlot_LegRight Auto Const Mandatory
EndGroup


; Events
;---------------------------------------------

Event OnQuestInit()
	Debug.TraceSelf(self, "OnQuestInit", "Event has been received.")
	Player = Game.GetPlayer()
	Player.AddItem(AutoDoc_Cyber_ImplantSlot_Head, ImplantSlot_Head, true)
	Player.AddItem(AutoDoc_Cyber_ImplantSlot_Body, ImplantSlot_Body, true)
	Player.AddItem(AutoDoc_Cyber_ImplantSlot_ArmLeft, ImplantSlot_ArmLeft, true)
	Player.AddItem(AutoDoc_Cyber_ImplantSlot_ArmRight, ImplantSlot_ArmRight, true)
	Player.AddItem(AutoDoc_Cyber_ImplantSlot_LegLeft, ImplantSlot_LegLeft, true)
	Player.AddItem(AutoDoc_Cyber_ImplantSlot_LegRight, ImplantSlot_LegRight, true)
EndEvent
