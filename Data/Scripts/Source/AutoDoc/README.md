The root directory for this Papyrus import.
All first level child directories here are considered the beginning of a Papyrus namespace.

This project's default import uses the `AutoDoc` identifier.
This project's default namespace uses the `AutoDoc` identifier.
