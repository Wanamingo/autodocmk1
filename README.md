# AutoDoc, The Automatic Doctor
<img src="Documents/AutoDoc.png" alt="Auto-Doc MKI" width="256" height="256" align="right">

An AutoDoc, abbreviation of "Automatic Doctor", is a miracle of pre-War medical technology.
The AutoDoc is a device capable of performing even the most complex medical procedures without qualified medical personnel.
Even for a talented physician, these are invaluable aids.

Early models were retroactively upgraded with new software and functionality introduced in later Auto-Docs before the Great War.
Even Mark I devices are commonly able to perform complex surgeries just as well as the Mark IX and Sink models.

Typically, AutoDoc functionality includes regenerating health, restoring crippled limbs and performing surgeries.
The AutoDoc is also capable of implementing implants that enhance both physical and mental performance.

Optional upgrades also allow it to mitigate fatigue (by injecting adrenaline - not recommended for extended use) and dispense medical supplies.
Even after the Great War, Auto-Docs have remained a rare technology found in the possession of only a few individuals.

# Project
The meta details about this project.

An in-game object which can be used by the player to heal health and limbs.
It could work with vanilla to fix limbs too at the cost of chance of infection unless you use antiseptic.

*Paul* made this *AutoDoc* bed mesh but it's currently unused, he said he wanted for it to be used somewhere.
The author *i_code_i* is animating Paul's AutoDoc mesh so it can be used as an activator.
*Flipdeezy* said it'd be cool for AutoDoc to work with his *Agony* mod so it can do surgery to fix bones.

* **Team:** AutoDoc Team
* **Start Date:** August 12th, 2019
* **Lore:** https://fallout.fandom.com/wiki/AutoDoc

**Team**
- Models: `Paul·#7415`
- Animations: `i_code_i#7527`
- Animations: `MaxG3D#4323`
- Data: `Wanamingo#5330`
- Data & Scripts: `Flipdeezy#9024`
- Scripts: `Scrivener07#4154`
- Collaborator: `Sagittarius#4935`
- Animation/Collision fixes: `KernalsEgg#3547`
- Animation fixes/Debugging assistance: `Sebbo#5783`

**Licensing**
This project has no explicit licensing.
The licensing details for this project will be discussed at a later date.
- Choose Project License (?)
- Monetization Allowed (?)
- Nexus DP Allowed (?)
- Rehosting Allowed (?)

**Support**
- Xbox Compatible
- Multi-language Localization Support
- Vanilla Compatible
- Vanilla Hardcore Mode Compatible
- Agony Compatible
- Sim Settlements Compatible

**Features**
- Custom Assets including Sounds, Models, and Animations
- Unique Object Interactions
- Unique Player Interactions
- Unique NPC Interactions
- World Object Placements
- Workshop Crafting
- Settlement Interactions

**Video**
- https://youtu.be/S-m5we8h_o4 (Chair spawn test)
- https://youtu.be/agkuccfvxt8 (Chair spawn test with Autodoc mesh)
- https://youtu.be/J2YkO-CRxKA (Chair point of view Proof-Of-Concept)
- https://youtu.be/crDQGzDtYhM (First hospital bed spawn test)
- https://youtu.be/yBiqcT61Db4 (Tube autodoc animation Proof-Of-Concept)
- https://youtu.be/ot7Sq9SA4lc (Bed test. Proving you can sleep in it when the Autodoc power is off)
- https://youtu.be/b_ytwQwiJj0 (Autodoc AVIF power on/off concept)
- https://youtu.be/pWSEEZ88gyo (Self check for terminal events)
- https://youtu.be/9ghW4AplkgM (First furniture animation test)
- https://youtu.be/JtUzVsqdcwI (Autodoc mesh animation test)
- https://youtu.be/65P-btuP3qU (WIP version, only works once after session reload)
- https://youtu.be/2o2oYc_F9FM (First MaxG3D assistance, looped animation)
- https://youtu.be/e-6rRnx2Kq8 (Animation playing after powering on the machine)
- https://youtu.be/hL-pMFLzv9g (Autodoc animation sound test)
- https://cdn.discordapp.com/attachments/404749722436501534/664491844616323122/2020-01-08_10-32-50.mp4 (Cybernetics bench proof-of-concept)


**Files**
- [Auto-Doc Project Sheets](https://docs.google.com/spreadsheets/d/1G3HHthIFDaYuyPXbSnqwKNJfKEiMPpFofHlP3SCyydI)
- [AutoDoc Textures and Materials](https://drive.google.com/file/d/1uNH92hB8Ymuo_wGqPHB9aosksyUlYi1-/view)
- [Autodocmk1_Alpha_v1_1.7z](https://www.dropbox.com/s/xsdcp3mr98c83ki/autodocmk1_Alpha_v1_1.7z)
- [Autodocmk1_Alpha2.0.7z](https://www.dropbox.com/s/myatp14h5gbgj5a/Autodocmk1_Alpha2.0.7z)
- [Autodocmk1_Alpha2.0.Scrivener.1](https://drive.google.com/file/d/18E5Nba_lfJ7kwxVU0A641O1XUKRupp70)
- [Autodocmk1_Alpha2.0.Scrivener.2](https://drive.google.com/file/d/1KxW4e_qaFvO_bD5Hp9Oa2TTjgrU2X5kM)

**Resources**
- [Health Icon by PinClipArt.com](https://www.pinclipart.com/pindetail/wowTJx_doctor-symbol-clipart-health-science-png-download/)
- [Agony by Meysam & Flipdeezy](https://www.nexusmods.com/fallout4/mods/25600)
- [DavidJCobb - Rotation Library](https://www.creationkit.com/index.php?title=User:DavidJCobb/Rotation_Library)
- [Kinggath - WarPlannersDesk.psc](https://cdn.discordapp.com/attachments/610261463780818970/610934007894507562/WarPlannersDesk.psc)

**Help & Documentation**
- https://forums.nexusmods.com/index.php?/topic/984792-tutorial-working-with-the-nicontrollermanager/
- [NifSkope Basics #3 Part 2 - Adding Simple Animations](https://youtu.be/CInLV8uir0w)

**Inspiration**
- [Courser Crusher](https://www.nexusmods.com/fallout4/mods/24981)
